function limitFunctionCallCount(cb, n) {
    let cb_executed = 0;

    function limit_cb_uptoN(){
        if(cb_executed<n){
            cb_executed++;
            return cb()
        }
    }
    return limit_cb_uptoN
}

module.exports = limitFunctionCallCount