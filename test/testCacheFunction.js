const cacheFunction = require('../cacheFunction')

// cb goes here or inline to cacheFunction

let sqr=function(x){
    return x*x
}

let result=cacheFunction(sqr)

console.log(result(5))
console.log(result(5))
console.log(result(5))
console.log(result(10))
console.log(result(10))


