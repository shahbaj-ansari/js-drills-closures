function cacheFunction(cb) {
    let cacheArray={}

    function makeCache(arguments){
     // console.log(`args ${arguments}`)
      if(cacheArray[arguments]!=undefined){
        return cacheArray[arguments]
      }
      console.log(`calculating for ${arguments}`)
      return (cacheArray[arguments]=cb(arguments))
    }
    return makeCache
}

module.exports=cacheFunction